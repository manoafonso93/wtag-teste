<?php

	include "../class/Connection.php";
	include "../class/DataBase.php";
	include "../_functions/_functions.php";

	$db = new DataBase();

	$array = Array();

	//SE NECESSÁRIO RETIRE A VARIÁVEL QUE NÃO VAI PARA O BANCO DO $_POST
	$acao = $_POST["acao"];
	unset($_POST["acao"]);

	//ESTE FOREACH PEGA TODAS AS VÁRIAVEIS DO $_POST PARA SALVAR NO BANCO
	foreach ($_POST as $key => $value) {
		$array[$key] = $value;
	}
	
	//SE NECESSÁRIO USE ESSA FUNÇÃO PARA PREPARAR A STRING PARA SER INSERIDA NO BANCO
	$array["name"] 			= str2db($array["name"]); 
	$array["email"] 		= str2db($array["email"]); 
	$array["password"] 		= str2db($array["password"]); 
	$array["user"] 			= str2db($array["user"]); 

	if($acao == "insert") $result = $db->insert("users", $array); //INSERE TODAS AS VARIÁVEIS NO BANCO
	if($acao == "update") $result = $db->update("users", $array); //INSERE TODAS AS VARIÁVEIS NO BANCO
    
    $mensagem = "Nome: ".$array['name']."<br>";
    $mensagem .= "Email: ".$array["email"]."<br><br>";
    $mensagem .= "Seguem os dados necessários para você fazer login no sistema:<br>Usuário: ".$array["user"];
    $mensagem .= "<br>Senha: ".$array["password"];
    $mensagem .= "<br><br>Obrigado por se cadastrar no nosso sistema!";

	$emailenviar = $array["email"];
    $destino = $emailenviar;
    $assunto = "Cadastro no sistema WT.AG";
 
    // É necessário indicar que o formato do e-mail é html
	$headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: no-reply <sistema@wtagencia.com.br>';
     
    $enviaremail = mail($destino, $assunto, $mensagem, $headers);

	if($result){ // SE DER CERTO
		if($acao == "insert"){ // SE FOR INSERT
			$msg = "Cadastro realizado com sucesso! Enviamos um email para você com seus dados para você não esquecer!";
		} else { // SE FOR UPDATE
			$msg = "Salvo com sucesso!";
		}
	} else { // SE DER ERRADO
		$msg = "Erro!\nPor favor, tente novamente mais tarde.";
	} 

	$url = "http://".get_domain()."";

?>

<script type="text/javascript">
	var msg = "<?php echo $msg; ?>";
	alert(msg);
	location.href="<?php echo $url; ?>";
</script>