<br>
	<div class="row">

	    <?php 

	    	$cond = " ORDER BY id DESC";
	    	include "_get_produtos.php";

	    	$maker->set_label("LISTA DE PRODUTOS");
	    	$maker->title();

	    	$maker->open_row(); //ABRE UMA LINHA

	    		$maker->set_col("3 offset-s9");
	    		$maker->set_icon("add");
	    		$maker->set_href("?o=prodform");
	    		$maker->set_label("Novo Produto");
	    		$maker->button();

	    	$maker->divide_row();

	    		$maker->open_table("ID,Produto,Preço,Status,Editar,Excluir");

	    			foreach ($aProdutos as $key => $value) {
	    				
	    				$maker->open_line();

	    					$maker->column($key);

	    					$maker->column($value);

	    					$maker->column($aPrecos[$key]);

	    					$maker->open_column();

	    						$maker->set_href("modules/_switch_produtos.php?i=".$key."&s=".$aStatus[$key]);
	    						if($aStatus[$key] == "1"){
		    						$maker->set_label("ATIVO (clique para desativar)");
		    						$maker->set_icon("visibility");
		    					}else{
		    						$maker->set_label("INATIVO (clique para ativar)");
		    						$maker->set_icon("visibility_off");
		    					}
	    						$maker->icon_button();

	    					$maker->close_column();

	    					$maker->open_column();

	    						$maker->set_href("?o=prodform&i=".$key);
	    						$maker->set_label("Clique para editar o produto.");
	    						$maker->set_icon("mode_edit");
	    						$maker->icon_button();

	    					$maker->close_column();

	    					$maker->open_column();

	    						$maker->set_href("modules/_delete_produtos.php?i=".$key);
	    						$maker->delete_button();

	    					$maker->close_column();

	    				$maker->close_line();

	    			}

	    		$maker->close_table();

	    	$maker->close_row();

	    ?>

	</div>
<br>