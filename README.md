# README #

Teste realizado para a WT.AG com CRUD de produtos e um painel simples de gerenciamento.

### About ###

* Version 1.0.0
* Created by Manoel Afonso
* https://www.linkedin.com/in/manoel-afonso-14496835/
* Coded in PHP

### Set Up ###

* Copie o conteúdo do repositório para o diretório desejado;
* Edite o arquivo "Connection.php" dentro da pasta "class" para adicionar os dados do seu banco;
* Crie o banco de dados MySQL no seu servidor;
* Importe o arquivo "wtag.sql" que está na pasta "_database" para o seu banco de dados;
* Go Test;