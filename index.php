<?php
    session_start();

    include "_functions/_maker.php";
    include "_functions/_functions.php";
    $maker = new Maker();    

    if($_SESSION["wtag_id"] == null || $_SESSION["wtag_id"] == ""){
      $msg = "Você precisa estar logado para acessar este painel!";
      $url = "http://".get_domain()."/login";
    }
    ?><script type="text/javascript">
        <?php if($msg != "") { ?>var msg = "<?php echo $msg; ?>";
        alert(msg);<?php }
        if($msg != "") { ?>location.href="<?php echo $url; ?>";
        <?php } ?>
    </script><?php

    $mobile = is_mobile();
?>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" href="css/estilo.css" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>WT.AG</title>
  </head>

  <style>
    .mobile-menu{
      font-size: 30px;
      margin-left: 10px;
      line-height: 50px;
      color: #ee6e73;
    }
    .mobile-top-bar{
      height: 50px;
    }
    .header {
      color: #ee6e73;
      font-weight: 200;
    }
    .new-account{
      width: 100%;
      background-color: #ee6e73;
    }
  </style>

  <body style="">
    <!--Import jQuery before materialize.js-->
    <!--<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>


    <script type="text/javascript"> 
    
      $(document).ready(function () { 
        $('select').material_select();
      });
      
    </script>

        <!-- MENU - MODO DESKTOP -->
        <nav style="position: fixed; z-index: 999; top:0; left: 0;">
          <div class="nav-wrapper" style="background-color: #0e8c4e;">
            <a href="?o=principal" class="brand-logo"><img src="images/logo.png" style="margin-left: 15px; margin-top: 10px; height: 45px;" /></a>
            <ul class="right hide-on-med-and-down">
              <li><a href="?o=prodlist">PRODUTOS</a></li>
              <li><a href="_logout.php">SAIR</a></li>
            </ul>
          </div>
        </nav>

    <div class="container" style="margin-top: 100px">
    <?php

      if(isset($_GET["o"])){
        require_once("modules/".$_GET["o"].".php");
      }else{
        require_once("modules/prodlist.php");
      }
      
    ?>
    </div>

  </body>
  <script type="text/javascript">
    // Show sideNav
    $('.button-collapse').sideNav('show');
    // Hide sideNav
    $('.button-collapse').sideNav('hide');
    // Destroy sideNav
    $('.button-collapse').sideNav('destroy');

    $('.button-collapse').sideNav({
        menuWidth: 300, // Default is 300
        edge: 'left', // Choose the horizontal origin
        closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
        draggable: true // Choose whether you can drag to open on touch screens
      }
    );
  </script>
</html>