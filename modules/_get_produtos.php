<? 
	include_once("class/Connection.php");
	include_once("class/DataBase.php");

	$db = new DataBase;

	$aProdutos = Array();
	$aPrecos = Array();
	$aStatus = Array();

	$result = $db->select("produtos", $cond);
	$cond = "";

	while($item = mysqli_fetch_array($result, MYSQLI_ASSOC)){ 
		$aProdutos[$item["id"]] 		= $item["nome"];
		$aPrecos[$item["id"]] 			= $item["preco"];
		$aStatus[$item["id"]] 			= $item["status"];
	}

?>