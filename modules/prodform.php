<br>
<div class="container">
	<div class="row">
	    <form class="col s12" action="modules/_submit_produtos.php" method="POST">

	    <?php 

	    	$id = $_GET["i"];

	    	if($id != ""){
	    		include_once("class/Connection.php");
				include_once("class/DataBase.php");

				$db = new DataBase;

				$result = $db->select("produtos"," WHERE id = '".$id."'");

				while($item = mysqli_fetch_array($result, MYSQLI_ASSOC)){ 
					$nome  					= db2str($item["nome"]);
					$descricao  			= db2str($item["descricao"]);
					$preco  				= $item["preco"];
					$status  				= $item["status"];
				}	
	    	}

	    	$maker->set_label("NOVO PRODUTO");
	    	$maker->title();

	    	$maker->open_row(); //ABRE UMA LINHA

	    		if($id != ""){
		    		$maker->set_name("id");
		    		$maker->set_value($id);
		    		$maker->input_hidden();

		    		$acao = "update";
	    		}else{
		    		$acao = "insert";
	    		}

	    		$maker->set_name("acao");
	    		$maker->set_value($acao);
	    		$maker->input_hidden();

	    		$maker->set_col("6");
	    		$maker->set_name("nome");
	    		$maker->set_label("Nome do Produto");
	    		$maker->set_value($nome);
	    		$maker->input_text(true); //ENVIAR 'TRUE' PARA CAMPO OBRIGATÓRIO

	    		$maker->set_col("3");
	    		$maker->set_name("preco");
	    		$maker->set_label("Preço (use ponto)");
	    		$maker->set_value($preco);
	    		$maker->input_text(true); //ENVIAR 'TRUE' PARA CAMPO OBRIGATÓRIO

                $maker->set_col("3");
                $maker->set_name("status");
                $maker->set_label("Status");
	    		if($id != "") $maker->set_value($status);
	    		if($id == "") $maker->set_value("1");
                $array = explode(",","NONE,ATIVO,INATIVO");
                unset($array["0"]);
                $maker->input_select($array, true); //ENVIAR 'TRUE' PARA CAMPO OBRIGATÓRIO

	    		$maker->set_col("12");
	    		$maker->set_name("descricao");
	    		$maker->set_label("Descrição");
	    		$maker->set_value($descricao);
	    		$maker->set_max("500");
	    		$maker->input_textarea(true); //ENVIAR 'TRUE' PARA CAMPO OBRIGATÓRIO

	    	$maker->divide_row();

	    		$maker->set_col("4 offset-s8"); //ITEM DE 4 COLUNAS COM ESPAÇO DE 8 COLUNAS
	    		$maker->set_name("submit");
	    		$maker->set_label("Salvar Produto");
	    		$maker->submit_button();

	    	$maker->close_row();

	    ?>

	    </form>
	</div>
</div>
<br>